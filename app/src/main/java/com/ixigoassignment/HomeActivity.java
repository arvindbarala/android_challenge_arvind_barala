package com.ixigoassignment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    private Button searchButton;
    private EditText originCode;
    private EditText desniationCode;
    static private EditText departDate;
    private EditText returnDate;

    /**
     * this is for no create new instance when clicked any view for better performance
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onClicks(view);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setTitle(R.string.ixigo);
        searchButton = (Button) findViewById(R.id.flightSearchButton);
        originCode = (EditText) findViewById(R.id.originCode);
        desniationCode = (EditText) findViewById(R.id.desinationCode);
        departDate = (EditText) findViewById(R.id.departDate);
        returnDate = (EditText) findViewById(R.id.returnDate);
        searchButton.setOnClickListener(clickListener);
        departDate.setOnClickListener(clickListener);
        returnDate.setOnClickListener(clickListener);
    }

    /**
     * @param view this is clicked view
     */
    private void onClicks(View view) {
        switch (view.getId()) {
            case R.id.flightSearchButton:
                clickOnSearchButton();
                break;
            case R.id.departDate:
                clickOnDepartDate(view);
                break;
            case R.id.returnDate:
                clickOnReturnDate(view);
                break;

        }

    }

    /**
     * this method for clik on search button and here send only 2 parameter origin code and d
     * destination code but ixixgo app need depart date
     */
    private void clickOnSearchButton() {
        String originCodeText = originCode.getText().toString();
        String desniationCodeText = desniationCode.getText().toString();
        if (TextUtils.isEmpty(originCodeText)) {
            Toast.makeText(getApplicationContext(), R.string.toast_of_not_enter_origin_airport, Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(desniationCodeText)) {
            Toast.makeText(getApplicationContext(), R.string.toast_of_not_enter_desination_airport, Toast.LENGTH_LONG).show();
        } else {
            Intent allFlightsIntent = new Intent(getApplicationContext(), AllFlights.class);
            allFlightsIntent.putExtra(CommonVars.ORIGIN_CODE, originCodeText);
            allFlightsIntent.putExtra(CommonVars.DESTINATION_CODE, desniationCodeText);
            startActivity(allFlightsIntent);


        }
    }

    /**
     * @param view clicked on return date view
     */
    private void clickOnReturnDate(View view) {
        showTruitonDatePickerDialog(view);

    }

    /**
     * @param view clicked on depart date view
     */
    private void clickOnDepartDate(View view) {
        showTruitonDatePickerDialog(view);

    }

    /**
     * @param v clikced viewed
     */

    public void showTruitonDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    /**
     * inner class static for batter performance
     */
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            departDate.setText(day + "/" + (month + 1) + "/" + year);
        }
    }
}

