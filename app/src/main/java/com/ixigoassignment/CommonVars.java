package com.ixigoassignment;

/**
 * Created by arvind.barala on 9/2/2017.
 */

public interface CommonVars {
    String ORIGIN_CODE = "originCode";
    String DESTINATION_CODE = "destinationCode";
    String DEPART_DATE = "departDate";
    String RETURN_DATE = "returnDate";
}
