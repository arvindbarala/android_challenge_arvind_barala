package com.ixigoassignment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by arvind.barala on 9/2/2017.
 */
public class AllFlights extends AppCompatActivity {
    private RelativeLayout prgressBar;
    private ListView listView;
    private ListArrayAdpter listArrayAdpter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flights);
        setTitle(R.string.flights);
        prgressBar = (RelativeLayout) findViewById(R.id.progressBar);
        listView = (ListView) findViewById(R.id.flightListView);
        String originCode = getIntent().getStringExtra(CommonVars.ORIGIN_CODE);
        String destiCode = getIntent().getStringExtra(CommonVars.DESTINATION_CODE);
        AllFlightDataInBackgroundThread Aty = new AllFlightDataInBackgroundThread();
        Aty.execute(originCode, destiCode);
    }

    private class AllFlightDataInBackgroundThread extends AsyncTask<Object, Object, Object> {

        @Override
        protected ArrayList<ReceiveFlightBean> doInBackground(Object... params) {
            String HTML_response = "";
            String originCode = params[0].toString();
            String destiCode = params[1].toString();
            ArrayList<ReceiveFlightBean> rf = null;
            try {

                URL url = new URL("http://www.mocky.io/v2/5979c6731100001e039edcb3");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                // open the stream and put it into BufferedReader
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    // System.out.println(inputLine);
                    HTML_response += inputLine;
                }
                br.close();
                JSONObject jsonObject = new JSONObject(HTML_response.toString());
                rf = parseDataAndStoreInTable(jsonObject);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return rf;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            prgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Object response) {
            super.onPostExecute(response);
            prgressBar.setVisibility(View.INVISIBLE);
            ListArrayAdpter listArrayAdpter = new ListArrayAdpter(getApplicationContext(),(ArrayList<ReceiveFlightBean>) response);
            listView.setAdapter(listArrayAdpter);
        }

        private ArrayList<ReceiveFlightBean> parseDataAndStoreInTable(JSONObject jsonObject) throws JSONException {
            JSONObject apendix = jsonObject.getJSONObject("appendix");
            HashMap<String, String> airLines = getHashMapFromObject(apendix.getJSONObject("airlines"));
            //HashMap<String, String> airports = getHashMapFromObject(apendix.getJSONObject("airports"));
            HashMap<String, String> providers = getHashMapFromObject(apendix.getJSONObject("providers"));
            JSONArray flighttsArray = jsonObject.getJSONArray("flights");
            ArrayList<ReceiveFlightBean> arrayOfFlightInfo = new ArrayList<>();
            for (int i = 0; i < flighttsArray.length(); i++) {
                JSONObject flightJson = flighttsArray.getJSONObject(i);
                ReceiveFlightBean receiveFlightBean = new ReceiveFlightBean();
                receiveFlightBean.originCode = flightJson.getString("originCode");
                receiveFlightBean.destinationCode = flightJson.getString("destinationCode");
                receiveFlightBean.departureTime = getTime(flightJson.getString("departureTime"));
                receiveFlightBean.arrivalTime = getTime(flightJson.getString("arrivalTime"));
                receiveFlightBean.airline = airLines.get(flightJson.getString("airlineCode"));
                receiveFlightBean.classType = flightJson.getString("class");
                JSONArray faresArray = flightJson.getJSONArray("fares");
                int length = faresArray.length();
                receiveFlightBean.provider = new String[length];
                receiveFlightBean.fare = new String[length];
                for (int j = 0; j < length; j++) {
                    JSONObject fareJson = faresArray.getJSONObject(j);
                    receiveFlightBean.provider[j] = providers.get(fareJson.getString("providerId"));
                    receiveFlightBean.fare[j] = fareJson.getString("fare");
                }
                arrayOfFlightInfo.add(receiveFlightBean);
            }
            return arrayOfFlightInfo;
        }

        private String getTime(String arrivalTime) {
            Long millis = Long.parseLong(arrivalTime);
            Date time=new Date(millis);
            SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
            String result = localDateFormat.format(time);
            return result;
        }

        private HashMap<String, String> getHashMapFromObject(JSONObject jsonObject) throws JSONException {
            HashMap<String, String> hashMap = new HashMap<>();
            Iterator<String> keys = jsonObject.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                hashMap.put(key, jsonObject.getString(key));
            }
            return hashMap;
        }


        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }
}
