package com.ixigoassignment.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;

public class CustomEditText extends AppCompatEditText implements CustomView {

    private ValidationListener validationListener;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    private void updateEditTextBackground(boolean showError) {

    }

    @Override
    public void setLevel(int levelValue) {

    }

    public void setErrorView() {
        updateEditTextBackground(true);
    }

    public void setNormalView() {
        updateEditTextBackground(false);
    }

    public void setDisabledView() {
        //setLevel(DISABLED_VIEW);
    }

    public void moveCursorToLast() {
        setSelection(getText().length());
    }

    public void setOnValidationListener(ValidationListener validationListener) {
        this.validationListener = validationListener;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (!focused && validationListener != null) {
            validationListener.onValidation(CustomEditText.this, getText().toString());
        }
    }

    public void removeValidationListener() {
        validationListener = null;
    }

    public interface ValidationListener {
        void onValidation(View view, CharSequence charSequence);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        ViewParent viewParent = this.getParent();
        if (viewParent instanceof TextInputLayout) {
            TextInputLayout textInputLayout = (TextInputLayout) viewParent;
            textInputLayout.setHintAnimationEnabled(false);
            super.setText(text, type);
            textInputLayout.setHintAnimationEnabled(true);
            return;
        }
        super.setText(text, type);
    }
}
