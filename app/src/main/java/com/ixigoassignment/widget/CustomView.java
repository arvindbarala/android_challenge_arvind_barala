package com.ixigoassignment.widget;

import android.graphics.drawable.Drawable;

public interface CustomView {
	int ERROR_VIEW = 1;
	int NORMAL_VIEW = 0;
	int DISABLED_VIEW = 3;

	/** Lower end phone supported */
	void setBackground(Drawable drawable);

	void setLevel(int levelValue);

	void setErrorView();

	void setNormalView();

}
