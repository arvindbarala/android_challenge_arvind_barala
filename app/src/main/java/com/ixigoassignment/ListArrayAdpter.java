package com.ixigoassignment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by arvind.barala on 9/2/2017.
 */
public class ListArrayAdpter extends ArrayAdapter<ReceiveFlightBean> {
    private Context context;

    public ListArrayAdpter(Context context, ArrayList<ReceiveFlightBean> receiveFlightBeen) {
        super(context, 0, receiveFlightBeen);
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflate = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = inflate.inflate(R.layout.list_item_flight, null);
        }
        ReceiveFlightBean receiveFlightBean = getItem(position);
        ((TextView)convertView.findViewById(R.id.airlaineNameText)).setText(receiveFlightBean.airline);
        ((TextView)convertView.findViewById(R.id.numberOfFlights)).setText(receiveFlightBean.provider.length+" flights");
        ((TextView)convertView.findViewById(R.id.arrivalTime)).setText(receiveFlightBean.arrivalTime);
        ((TextView)convertView.findViewById(R.id.departTime)).setText(receiveFlightBean.departureTime);
        ((TextView)convertView.findViewById(R.id.totalTime)).setText("1h 25m");
        ((TextView)convertView.findViewById(R.id.numberOfStop)).setText("non stop");
        ((TextView)convertView.findViewById(R.id.fareText)).setText(receiveFlightBean.fare[0]);
        return convertView;
    }
}
