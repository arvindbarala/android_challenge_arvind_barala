package com.ixigoassignment;

/**
 * Created by arvind.barala on 9/2/2017.
 */
public class ReceiveFlightBean {
    String originCode;
    String destinationCode;
    String departureTime;
    String arrivalTime;
    String[] provider;
    String[] fare;
    String airline;
    String classType;
}
